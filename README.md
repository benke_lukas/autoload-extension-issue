This repo contains 3 folders:

dashboard - extensions created manually - not even single file can be autoloaded - 
replicate by: 
```
php artisan extension:autoload extension=akon/dashboard

php artisan tinker

new Akon\Dashboard\Controllers\DashboardController
```

dashboardscaffolded - extensions created by workshop UI - not even single file can be autoloaded - 
replicate by: 
```
php artisan extension:autoload extension=akon/dashboardscaffolded

php artisan tinker

new Akon\Dashboardscaffolded\Controllers\ScaffoldedDashboardController
```

fullyscaffoldeddashboard - extensions created by workshop UI and scaffolded everything - here, everything works:
```
php artisan extension:autoload extension=akon/fullyscaffoldeddashboard

php artisan tinker

new Akon\Fullyscaffoldeddashboard\Controllers\Admin\DashboardsController
```