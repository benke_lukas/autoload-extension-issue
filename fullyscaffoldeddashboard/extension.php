<?php

use Cartalyst\Extensions\ExtensionInterface;
use Cartalyst\Settings\Repository as Settings;
use Illuminate\Contracts\Foundation\Application;
use Cartalyst\Permissions\Container as Permissions;
use Illuminate\Contracts\Routing\Registrar as Router;

return [

    /*
    |--------------------------------------------------------------------------
    | Name
    |--------------------------------------------------------------------------
    |
    | This is your extension name and it is only required for
    | presentational purposes.
    |
    */

    'name' => 'Fullyscaffoldeddashboard',

    /*
    |--------------------------------------------------------------------------
    | Slug
    |--------------------------------------------------------------------------
    |
    | This is your extension unique identifier and should not be changed as
    | it will be recognized as a new extension.
    |
    | Ideally, this should match the folder structure within the extensions
    | folder, but this is completely optional.
    |
    */

    'slug' => 'akon/fullyscaffoldeddashboard',

    /*
    |--------------------------------------------------------------------------
    | Author
    |--------------------------------------------------------------------------
    |
    | Because everybody deserves credit for their work, right?
    |
    */

    'author' => 'Lukáš Benke',

    /*
    |--------------------------------------------------------------------------
    | Description
    |--------------------------------------------------------------------------
    |
    | One or two sentences describing the extension for users to view when
    | they are installing the extension.
    |
    */

    'description' => 'Fullyscaffolded dashboard',

    /*
    |--------------------------------------------------------------------------
    | Version
    |--------------------------------------------------------------------------
    |
    | Version should be a string that can be used with version_compare().
    | This is how the extensions versions are compared.
    |
    */

    'version' => '0.1.0',

    /*
    |--------------------------------------------------------------------------
    | Requirements
    |--------------------------------------------------------------------------
    |
    | List here all the extensions that this extension requires to work.
    | This is used in conjunction with composer, so you should put the
    | same extension dependencies on your main composer.json require
    | key, so that they get resolved using composer, however you
    | can use without composer, at which point you'll have to
    | ensure that the required extensions are available.
    |
    */

    'requires' => [
		'platform/dashboard',
	],

    /*
    |--------------------------------------------------------------------------
    | Autoload Logic
    |--------------------------------------------------------------------------
    |
    | You can define here your extension autoloading logic, it may either
    | be 'composer', 'platform' or a 'Closure'.
    |
    | If composer is defined, your composer.json file specifies the autoloading
    | logic.
    |
    | If platform is defined, your extension receives convetion autoloading
    | based on the Platform standards.
    |
    | If a Closure is defined, it should take two parameters as defined
    | bellow:
    |
    |   object \Composer\Autoload\ClassLoader      $loader
    |   object \Illuminate\Foundation\Application  $app
    |
    | Supported: "composer", "platform", "Closure"
    |
    */

    'autoload' => 'composer',

    /*
    |--------------------------------------------------------------------------
    | Service Providers
    |--------------------------------------------------------------------------
    |
    | Define your extension service providers here. They will be dynamically
    | registered without having to include them in app/config/app.php.
    |
    */

    'providers' => [

        'Akon\Fullyscaffoldeddashboard\Providers\DashboardServiceProvider',

    ],

    /*
    |--------------------------------------------------------------------------
    | Routes
    |--------------------------------------------------------------------------
    |
    | Closure that is called when the extension is started. You can register
    | any custom routing logic here.
    |
    | The closure parameters are:
    |
    |   object \Illuminate\Contracts\Routing\Registrar  $router
    |   object \Cartalyst\Extensions\ExtensionInterface  $extension
    |   object \Illuminate\Contracts\Foundation\Application  $app
    |
    */

    'routes' => function(Router $router, ExtensionInterface $extension, Application $app) {
        $router->group(['namespace' => 'Akon\Fullyscaffoldeddashboard\Controllers\Admin'], function (Router $router){
            $router->group(['prefix' => admin_uri().'/fullyscaffoldeddashboard/dashboards'], function (Router $router){
                $router->get('/', 'DashboardsController@index')->name('admin.akon.fullyscaffoldeddashboard.dashboards.all');
                $router->post('/', 'DashboardsController@executeAction')->name('admin.akon.fullyscaffoldeddashboard.dashboards.all');

                $router->get('grid', 'DashboardsController@grid')->name('admin.akon.fullyscaffoldeddashboard.dashboards.grid');

                $router->get('create' , 'DashboardsController@create')->name('admin.akon.fullyscaffoldeddashboard.dashboards.create');
                $router->post('create', 'DashboardsController@store')->name('admin.akon.fullyscaffoldeddashboard.dashboards.create');

                $router->get('{id}', 'DashboardsController@edit')->name('admin.akon.fullyscaffoldeddashboard.dashboards.edit');
                $router->post('{id}', 'DashboardsController@update')->name('admin.akon.fullyscaffoldeddashboard.dashboards.edit');

                $router->delete('{id}', 'DashboardsController@delete')->name('admin.akon.fullyscaffoldeddashboard.dashboards.delete');
            });
        });

        $router->group(['namespace' => 'Akon\Fullyscaffoldeddashboard\Controllers\Frontend'], function (Router $router){
            $router->group(['prefix' => 'fullyscaffoldeddashboard/dashboards'], function (Router $router){
                $router->get('/', 'DashboardsController@index')->name('akon.fullyscaffoldeddashboard.dashboards.index');
            });
        });
    },

    /*
    |--------------------------------------------------------------------------
    | Permissions
    |--------------------------------------------------------------------------
    |
    | Register here all the permissions that this extension has. These will
    | be shown in the user management area to build a graphical interface
    | where permissions can be selected to allow or deny user access.
    |
    | For detailed instructions on how to register the permissions, please
    | refer to the following url https://cartalyst.com/manual/permissions
    |
    | The closure parameters are:
    |
    |   object \Cartalyst\Permissions\Container  $permissions
    |   object \Illuminate\Contracts\Foundation\Application  $app
    |
    */

    'permissions' => function (Permissions $permissions, Application $app) {
        $permissions->group('dashboard', function($g) {
            $g->name = 'Dashboards';

            $g->permission('dashboard.index', function($p) {
                $p->label = trans('akon/fullyscaffoldeddashboard::dashboards/permissions.index');

                $p->controller('Akon\Fullyscaffoldeddashboard\Controllers\Admin\DashboardsController', 'index, grid');
            });

            $g->permission('dashboard.create', function($p) {
                $p->label = trans('akon/fullyscaffoldeddashboard::dashboards/permissions.create');

                $p->controller('Akon\Fullyscaffoldeddashboard\Controllers\Admin\DashboardsController', 'create, store');
            });

            $g->permission('dashboard.edit', function($p) {
                $p->label = trans('akon/fullyscaffoldeddashboard::dashboards/permissions.edit');

                $p->controller('Akon\Fullyscaffoldeddashboard\Controllers\Admin\DashboardsController', 'edit, update');
            });

            $g->permission('dashboard.delete', function($p) {
                $p->label = trans('akon/fullyscaffoldeddashboard::dashboards/permissions.delete');

                $p->controller('Akon\Fullyscaffoldeddashboard\Controllers\Admin\DashboardsController', 'delete');
            });
        });
    },

    /*
    |--------------------------------------------------------------------------
    | Settings
    |--------------------------------------------------------------------------
    |
    | Register here all the settings that this extension has.
    |
    | For detailed instructions on how to register the settings, please
    | refer to the following url https://cartalyst.com/manual/settings
    |
    | The closure parameters are:
    |
    |   object \Cartalyst\Settings\Repository  $settings
    |   object \Illuminate\Contracts\Foundation\Application  $app
    |
    */

    'settings' => function (Settings $settings, Application $app) {

    },

    /*
    |--------------------------------------------------------------------------
    | Widgets
    |--------------------------------------------------------------------------
    |
    | Closure that is called when the extension is started. You can register
    | all your custom widgets here. Of course, Platform will guess the
    | widget class for you, this is just for custom widgets or if you
    | do not wish to make a new class for a very small widget.
    |
    */

    'widgets' => function() {

    },

    /*
    |--------------------------------------------------------------------------
    | Menus
    |--------------------------------------------------------------------------
    |
    | You may specify the default various menu hierarchy for your extension.
    | You can provide a recursive array of menu children and their children.
    | These will be created upon installation, synchronized upon upgrading
    | and removed upon uninstallation.
    |
    | Menu children are automatically put at the end of the menu for extensions
    | installed through the Operations extension.
    |
    | The default order (for extensions installed initially) can be
    | found by editing app/config/platform.php.
    |
    */

    'menus' => [

		'admin' => [
			[
				'slug' => 'admin-akon-fullyscaffoldeddashboard',
				'name' => 'Fullyscaffoldeddashboard',
				'class' => 'fa fa-circle-o',
				'uri' => 'fullyscaffoldeddashboard',
				'regex' => '/:admin\/fullyscaffoldeddashboard/i',
				'children' => [
					[
						'class' => 'fa fa-circle-o',
						'name' => 'Dashboards',
						'uri' => 'fullyscaffoldeddashboard/dashboards',
						'regex' => '/:admin\/fullyscaffoldeddashboard\/dashboard/i',
						'slug' => 'admin-akon-fullyscaffoldeddashboard-dashboard',
					],
				],
			],
		],
		'main' => [
			
		],
	],

];
