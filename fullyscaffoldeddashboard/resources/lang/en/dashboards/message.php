<?php

return [

	// General messages
	'not_found' => 'Dashboard [:id] neexistuje.',

	// Success messages
	'success' => [
		'create' => 'Dashboard byl vytvořen.',
		'update' => 'Dashboard byl upraven.',
		'delete' => 'Dashboard byl smazán.',
	],

	// Error messages
	'error' => [
		'create' => 'Nepovedlo se vytvořit dashboard. Zkuste to prosím znovu.',
		'update' => 'Nepovedlo se upravit dashboard. Zkuste to prosím znovu.',
		'delete' => 'Nepovedlo se smazat dashboard. Zkuste to prosím znovu.',
	],

];
