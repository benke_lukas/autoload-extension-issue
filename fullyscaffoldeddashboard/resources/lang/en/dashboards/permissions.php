<?php

return [

	'index'  => 'Zobrazit Dashboards',
	'create' => 'Vytvořit nový Dashboard',
	'edit'   => 'Upravit Dashboard',
	'delete' => 'Smazat Dashboard',

];
