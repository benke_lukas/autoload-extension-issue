<script type="text/template" data-grid="dashboard" data-grid-template="results">

    <% var results = response.results; %>

    <% if (_.isEmpty(results)) { %>

        <tr>
            <td colspan="5">{!! trans('common.no_results') !!}</td>
        </tr>

    <% } else { %>

        <% _.each(results, function(r) { %>

            <tr data-grid-row>
                <td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
			<td><a href="<%= r.edit_uri %>"><%= r.id %></a></td>
			<td><%= r.dashboard %></td>
			<td><%= r.created_at %></td>
            </tr>

        <% }); %>

    <% } %>

</script>
