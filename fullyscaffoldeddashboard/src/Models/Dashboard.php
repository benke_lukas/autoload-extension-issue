<?php

namespace Akon\Fullyscaffoldeddashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Attributes\EntityInterface;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;

class Dashboard extends Model implements EntityInterface
{
    use EntityTrait, NamespacedEntityTrait;

    /**
     * {@inheritDoc}
     */
    protected $table = 'dashboards';

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $with = [
        'values.attribute',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'akon/fullyscaffoldeddashboard.dashboard';
}
