<?php

namespace Akon\Fullyscaffoldeddashboard\Providers;

use Cartalyst\Support\ServiceProvider;

class DashboardServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        // Register the attributes namespace
        $this->app['platform.attributes.manager']->registerNamespace(
            $this->app['Akon\Fullyscaffoldeddashboard\Models\Dashboard']
        );

        // Subscribe the registered event handler
        $this->app['events']->subscribe('akon.fullyscaffoldeddashboard.dashboard.handler.event');
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        // Register the repository
        $this->bindIf('akon.fullyscaffoldeddashboard.dashboard', 'Akon\Fullyscaffoldeddashboard\Repositories\Dashboard\DashboardRepository');

        // Register the data handler
        $this->bindIf('akon.fullyscaffoldeddashboard.dashboard.handler.data', 'Akon\Fullyscaffoldeddashboard\Handlers\Dashboard\DashboardDataHandler');

        // Register the event handler
        $this->bindIf('akon.fullyscaffoldeddashboard.dashboard.handler.event', 'Akon\Fullyscaffoldeddashboard\Handlers\Dashboard\DashboardEventHandler');

        // Register the validator
        $this->bindIf('akon.fullyscaffoldeddashboard.dashboard.validator', 'Akon\Fullyscaffoldeddashboard\Validator\Dashboard\DashboardValidator');
    }
}
