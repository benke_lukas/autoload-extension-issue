<?php

namespace Akon\Fullyscaffoldeddashboard\Repositories\Dashboard;

use Cartalyst\Support\Traits;
use Symfony\Component\Finder\Finder;
use Illuminate\Contracts\Container\Container;

class DashboardRepository implements DashboardRepositoryInterface
{
    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Data handler.
     *
     * @var \Akon\Fullyscaffoldeddashboard\Handlers\Dashboard\DashboardDataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent fullyscaffoldeddashboard model.
     *
     * @var string
     */
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Contracts\Container\Container  $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->data = $app['akon.fullyscaffoldeddashboard.dashboard.handler.data'];

        $this->setValidator($app['akon.fullyscaffoldeddashboard.dashboard.validator']);

        $this->setModel(get_class($app['Akon\Fullyscaffoldeddashboard\Models\Dashboard']));
    }

    /**
     * {@inheritDoc}
     */
    public function grid()
    {
        return $this
            ->createModel();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->container['cache']->rememberForever('akon.fullyscaffoldeddashboard.dashboard.all', function() {
            return $this->createModel()->get();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function find($id)
    {
        return $this->container['cache']->rememberForever('akon.fullyscaffoldeddashboard.dashboard.'.$id, function() use ($id) {
            return $this->createModel()->find($id);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input)
    {
        return $this->validator->on('create')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($id, array $input)
    {
        return $this->validator->on('update')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input)
    {
        return ! $id ? $this->create($input) : $this->update($id, $input);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input)
    {
        // Create a new dashboard
        $dashboard = $this->createModel();

        // Fire the 'akon.fullyscaffoldeddashboard.dashboard.creating' event
        if ($this->fireEvent('akon.fullyscaffoldeddashboard.dashboard.creating', [ $input ]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForCreation($data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Save the dashboard
            $dashboard->fill($data)->save();

            // Fire the 'akon.fullyscaffoldeddashboard.dashboard.created' event
            $this->fireEvent('akon.fullyscaffoldeddashboard.dashboard.created', [ $dashboard ]);
        }

        return [ $messages, $dashboard ];
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input)
    {
        // Get the dashboard object
        $dashboard = $this->find($id);

        // Fire the 'akon.fullyscaffoldeddashboard.dashboard.updating' event
        if ($this->fireEvent('akon.fullyscaffoldeddashboard.dashboard.updating', [ $dashboard, $input ]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForUpdate($dashboard, $data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Update the dashboard
            $dashboard->fill($data)->save();

            // Fire the 'akon.fullyscaffoldeddashboard.dashboard.updated' event
            $this->fireEvent('akon.fullyscaffoldeddashboard.dashboard.updated', [ $dashboard ]);
        }

        return [ $messages, $dashboard ];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id)
    {
        // Check if the dashboard exists
        if ($dashboard = $this->find($id))
        {
            // Fire the 'akon.fullyscaffoldeddashboard.dashboard.deleting' event
            $this->fireEvent('akon.fullyscaffoldeddashboard.dashboard.deleting', [ $dashboard ]);

            // Delete the dashboard entry
            $dashboard->delete();

            // Fire the 'akon.fullyscaffoldeddashboard.dashboard.deleted' event
            $this->fireEvent('akon.fullyscaffoldeddashboard.dashboard.deleted', [ $dashboard ]);

            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function enable($id)
    {
        $this->validator->bypass();

        return $this->update($id, [ 'enabled' => true ]);
    }

    /**
     * {@inheritDoc}
     */
    public function disable($id)
    {
        $this->validator->bypass();

        return $this->update($id, [ 'enabled' => false ]);
    }
}
