<?php

namespace Akon\Fullyscaffoldeddashboard\Repositories\Dashboard;

interface DashboardRepositoryInterface
{
    /**
     * Returns a dataset compatible with data grid.
     *
     * @return \Akon\Fullyscaffoldeddashboard\Models\Dashboard
     */
    public function grid();

    /**
     * Returns all the fullyscaffoldeddashboard entries.
     *
     * @return \Akon\Fullyscaffoldeddashboard\Models\Dashboard
     */
    public function findAll();

    /**
     * Returns a fullyscaffoldeddashboard entry by its primary key.
     *
     * @param  int  $id
     * @return \Akon\Fullyscaffoldeddashboard\Models\Dashboard
     */
    public function find($id);

    /**
     * Determines if the given fullyscaffoldeddashboard is valid for creation.
     *
     * @param  array  $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForCreation(array $data);

    /**
     * Determines if the given fullyscaffoldeddashboard is valid for update.
     *
     * @param  int  $id
     * @param  array  $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForUpdate($id, array $data);

    /**
     * Creates or updates the given fullyscaffoldeddashboard.
     *
     * @param  int  $id
     * @param  array  $input
     * @return bool|array
     */
    public function store($id, array $input);

    /**
     * Creates a fullyscaffoldeddashboard entry with the given data.
     *
     * @param  array  $data
     * @return \Akon\Fullyscaffoldeddashboard\Models\Dashboard
     */
    public function create(array $data);

    /**
     * Updates the fullyscaffoldeddashboard entry with the given data.
     *
     * @param  int  $id
     * @param  array  $data
     * @return \Akon\Fullyscaffoldeddashboard\Models\Dashboard
     */
    public function update($id, array $data);

    /**
     * Deletes the fullyscaffoldeddashboard entry.
     *
     * @param  int  $id
     * @return bool
     */
    public function delete($id);
}
