<?php

namespace Akon\Fullyscaffoldeddashboard\Handlers\Dashboard;

interface DashboardDataHandlerInterface
{
    /**
     * Prepares the given data for being stored.
     *
     * @param  array  $data
     * @return mixed
     */
    public function prepare(array $data);
}
