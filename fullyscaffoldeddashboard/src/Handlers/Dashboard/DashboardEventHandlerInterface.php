<?php

namespace Akon\Fullyscaffoldeddashboard\Handlers\Dashboard;

use Akon\Fullyscaffoldeddashboard\Models\Dashboard;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface DashboardEventHandlerInterface extends BaseEventHandlerInterface
{
    /**
     * When a dashboard is being created.
     *
     * @param  array  $data
     * @return mixed
     */
    public function creating(array $data);

    /**
     * When a dashboard is created.
     *
     * @param  \Akon\Fullyscaffoldeddashboard\Models\Dashboard  $dashboard
     * @return mixed
     */
    public function created(Dashboard $dashboard);

    /**
     * When a dashboard is being updated.
     *
     * @param  \Akon\Fullyscaffoldeddashboard\Models\Dashboard  $dashboard
     * @param  array  $data
     * @return mixed
     */
    public function updating(Dashboard $dashboard, array $data);

    /**
     * When a dashboard is updated.
     *
     * @param  \Akon\Fullyscaffoldeddashboard\Models\Dashboard  $dashboard
     * @return mixed
     */
    public function updated(Dashboard $dashboard);

    /**
     * When a dashboard is being deleted.
     *
     * @param  \Akon\Fullyscaffoldeddashboard\Models\Dashboard  $dashboard
     * @return mixed
     */
    public function deleting(Dashboard $dashboard);

    /**
     * When a dashboard is deleted.
     *
     * @param  \Akon\Fullyscaffoldeddashboard\Models\Dashboard  $dashboard
     * @return mixed
     */
    public function deleted(Dashboard $dashboard);
}
