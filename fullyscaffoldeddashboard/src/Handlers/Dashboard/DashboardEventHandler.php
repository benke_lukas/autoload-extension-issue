<?php

namespace Akon\Fullyscaffoldeddashboard\Handlers\Dashboard;

use Illuminate\Contracts\Events\Dispatcher;
use Akon\Fullyscaffoldeddashboard\Models\Dashboard;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class DashboardEventHandler extends BaseEventHandler implements DashboardEventHandlerInterface
{
    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher)
    {
        $dispatcher->listen('akon.fullyscaffoldeddashboard.dashboard.creating', __CLASS__.'@creating');
        $dispatcher->listen('akon.fullyscaffoldeddashboard.dashboard.created', __CLASS__.'@created');

        $dispatcher->listen('akon.fullyscaffoldeddashboard.dashboard.updating', __CLASS__.'@updating');
        $dispatcher->listen('akon.fullyscaffoldeddashboard.dashboard.updated', __CLASS__.'@updated');

        $dispatcher->listen('akon.fullyscaffoldeddashboard.dashboard.deleted', __CLASS__.'@deleting');
        $dispatcher->listen('akon.fullyscaffoldeddashboard.dashboard.deleted', __CLASS__.'@deleted');
    }

    /**
     * {@inheritDoc}
     */
    public function creating(array $data)
    {

    }

    /**
     * {@inheritDoc}
     */
    public function created(Dashboard $dashboard)
    {
        $this->flushCache($dashboard);
    }

    /**
     * {@inheritDoc}
     */
    public function updating(Dashboard $dashboard, array $data)
    {

    }

    /**
     * {@inheritDoc}
     */
    public function updated(Dashboard $dashboard)
    {
        $this->flushCache($dashboard);
    }

    /**
     * {@inheritDoc}
     */
    public function deleting(Dashboard $dashboard)
    {

    }

    /**
     * {@inheritDoc}
     */
    public function deleted(Dashboard $dashboard)
    {
        $this->flushCache($dashboard);
    }

    /**
     * Flush the cache.
     *
     * @param  \Akon\Fullyscaffoldeddashboard\Models\Dashboard  $dashboard
     * @return void
     */
    protected function flushCache(Dashboard $dashboard)
    {
        $this->app['cache']->forget('akon.fullyscaffoldeddashboard.dashboard.all');

        $this->app['cache']->forget('akon.fullyscaffoldeddashboard.dashboard.'.$dashboard->id);
    }
}
