<?php

namespace Akon\Fullyscaffoldeddashboard\Validator\Dashboard;

interface DashboardValidatorInterface
{
    /**
     * Updating a dashboard scenario.
     *
     * @return void
     */
    public function onUpdate();
}
