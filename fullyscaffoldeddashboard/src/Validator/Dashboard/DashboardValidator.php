<?php

namespace Akon\Fullyscaffoldeddashboard\Validator\Dashboard;

use Cartalyst\Support\Validator;

class DashboardValidator extends Validator implements DashboardValidatorInterface
{
    /**
     * {@inheritDoc}
     */
    protected $rules = [

    ];

    /**
     * {@inheritDoc}
     */
    public function onUpdate()
    {

    }
}
